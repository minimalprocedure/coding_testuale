(TeX-add-style-hook
 "index"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "italian") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "babel"
    "fontenc"
    "fontspec"
    "graphicx"
    "mathtools"
    "algpseudocode"
    "algorithm"
    "listings"
    "minted"
    "hyperref"
    "ulem"
    "tikz"
    "lastpage"
    "fancyhdr"
    "geometry"
    "enumitem")
   (TeX-add-symbols
    "dontdofcolorbox")
   (LaTeX-add-labels
    "sec:orgd421ae1"
    "sec:org4868627"
    "sec:org60b1b53"
    "sec:org0b0feaf"
    "sec:orgc2cb2b1"
    "sec:orgd4b4abb"
    "sec:org4213f18"
    "sec:org24c2cc0"
    "sec:orgf3a8ad8"
    "sec:org0eb7c5a"
    "sec:org76c7c2e"
    "sec:orgd16016b"
    "sec:org7261041"
    "sec:org616f9fc"
    "sec:orgae69854"
    "sec:org36b4098"
    "sec:orgbf9fc15"
    "sec:orga02ffb6"
    "sec:org5931358"
    "sec:org7baf277"
    "sec:orgab44038"
    "sec:orga64a3a0"
    "sec:org5920c21"
    "sec:org267379a"
    "sec:orgc8da886"
    "sec:orgab2510c"
    "sec:org78c85fa"
    "sec:org687557f"
    "sec:org508d851"
    "sec:orga05d84e"
    "sec:org347f826"
    "sec:org5df2a54"
    "sec:org7b8de75"
    "sec:orgfd311fa"
    "sec:orge27d8a8"
    "sec:orgeea2777"
    "sec:orgbab36a8"
    "sec:org577a790"
    "sec:org4649afe"
    "sec:org8bf28c5"
    "sec:orgfc3421e"
    "sec:orga49bddf"
    "sec:orgac1d8b0"
    "sec:orge7072be"
    "sec:org65a927b"
    "sec:orgc2af3a1"
    "sec:org9976b92"
    "sec:org70b3e95"
    "sec:org0071107"
    "sec:org75ad31b"
    "sec:orgaa4cfaf"))
 :latex)

