clear
invisibile

def questo_o_quello(test: Boolean) (questo: => Unit) (quello: => Unit) {
  if(test) questo else quello
}

val n = 2

questo_o_quello(n > 3) {
  println("è questo")
} {
  println("è quello")
}
