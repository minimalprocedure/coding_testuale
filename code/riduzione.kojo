

def applica_la_funzione_alla_lista_di_numeri(
  f:     (Int, Int) => Int,
  lista: List[Int]) =
  lista.reduce(f)

def applica_la_funzione_alla_lista[T](
  f:     (T, T) => T,
  lista: List[T]) =
  lista.reduce(f)

def somma(a: Int, b: Int) = a + b
def sottrazione(a: Int, b: Int) = a - b
def divisione(a: Int, b: Int) = a / b
def resto(a: Int, b: Int) = a % b
def moltiplicazione(a: Int, b: Int) = a * b

val lista = List(1, 2, 3, 4, 5)

var res = applica_la_funzione_alla_lista_di_numeri(somma, lista)
println("somma: " + res.toString)

res = applica_la_funzione_alla_lista_di_numeri(sottrazione, lista)
println("sottrazione: " + res.toString)

res = applica_la_funzione_alla_lista_di_numeri(divisione, lista)
println("divisione: " + res.toString)

res = applica_la_funzione_alla_lista_di_numeri(resto, lista)
println("resto: " + res.toString)

res = applica_la_funzione_alla_lista_di_numeri(moltiplicazione, lista)
println("moltiplicazione: " + res.toString)