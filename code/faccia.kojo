pulisci()

def quadrato(t: Tartaruga, n: Int, delay: Int) {
    t.ritardo(delay)
    repeat(4) {
        t.avanti(n)
        t.destra()
    }
}

def occhi(t: Tartaruga, n: Int, delay: Int) {
    quadrato(t, n, delay)
    t.alzaPenna()
    t.avanti(n / 4)
    t.destra()
    t.avanti(n / 4)
    t.sinistra()
    t.abbassaPenna()
    t.coloreRiempimento(darkGray)
    quadrato(t, n / 2, delay)
}

val viso = nuovaTartaruga(-100, -100)
val occhioSinistro = nuovaTartaruga(-75, 25)
val occhioDestro = nuovaTartaruga(25, 25)
val bocca = nuovaTartaruga(-50, -50)
val naso = nuovaTartaruga(0, -25)
val capelli = nuovaTartaruga(-110, 100)
val corpo = nuovaTartaruga(25, -125)
val gambe = nuovaTartaruga(0, -150)

viso.fai { self => 
    self.coloreRiempimento(red)
    quadrato(self, 200, 200)
    self.invisibile()
}

occhioSinistro.fai { self =>
    self.coloreRiempimento(verde)
    occhi(self, 50, 800)
    self.invisibile()
}

occhioDestro.fai { self =>
    self.coloreRiempimento(giallo)
    occhi(self, 50, 800)
    self.invisibile()
}

bocca.fai { self =>
    self.ritardo(2000)
    self.colorePenna(yellow)
    self.impostaSpessorePenna(14)
    self.destra()
    self.avanti(100)
    self.invisibile()
}

naso.fai { self =>
    self.ritardo(4000)
    self.colorePenna(yellow)
    self.impostaSpessorePenna(20)
    self.avanti(50)
    self.invisibile()
}

capelli.fai { self =>
    self.ritardo(200)
    self.destra()
    self.colorePenna(black)
    self.impostaSpessorePenna(30)
    self.avanti(220)
    self.sinistra()
    repeat(10) {
        self.avanti(25)
        self.indietro(25)
        self.sinistra()
        self.avanti(22)
        self.destra()
    }
    self.avanti(25)
    self.invisibile()
}

corpo.fai { self => 
    self.coloreRiempimento(yellow)
    self.cerchio(25)
    self.invisibile()
}

gambe.fai { self => 
    self.ritardo(3000)
    self.colorePenna(black)
    self.impostaSpessorePenna(30)
    self.destra(180)
    self.salvaPosizioneDirezione()
    self.destra(30)
    self.avanti(30)
    self.ripristinaPosizioneDirezione()
    self.sinistra(30)
    self.avanti(30)
    self.invisibile()
}