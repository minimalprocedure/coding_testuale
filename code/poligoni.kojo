/* -*- Scala -*- */

pulisci();
invisibile();
ritardo(200);

def poligono(lato: Double, lati: Int): Double = {
  val angolo = 360 / lati.toDouble;
  seVero(lati % 2 >= 0) {
    sinistra(90 - angolo);
  }
  colorePenna(nero);
  ripeti(lati) {
    avanti(lato);
    destra(angolo);
  }
  perimetro;
}

def angoloPoligono(lato: Double, lati: Int): Double = {
  val sommaAngoli = (lati - 2) * 180;
  sommaAngoli / lati;
}

def raggio(lato: Double, lati: Int): Double = {
  val angolo = angoloPoligono(lato, lati);
  val mezzoLato = lato / 2;
  val coseno = Math.cos(Math.toRadians(angolo / 2));
  mezzoLato / coseno;
}

def disegnaRaggio(lato: Double, lati: Int): Double = {
  val r = raggio(lato, lati);
  val a = angoloPoligono(lato, lati);
  destra(a / 2);
  colorePenna(verde)
  avanti(r);
  disegnaCerchio(posizione, r, marrone);
  r;
}

def disegnaCerchio(posizione: Point, raggio: Double, colore: Color) {
  val t = nuovaTartaruga(posizione.x, posizione.y);
  t.ritardo(200);
  t.invisibile();
  t.alzaPenna();
  t.destra(90);
  t.avanti(raggio);
  t.sinistra(90);
  t.abbassaPenna();
  t.colorePenna(colore);
  t.cerchio(raggio);
}

def disegnaApotema(lato: Double, lati: Int): Double = {
  val ipotenusa = raggio(lato, lati);
  val base = lato / 2;
  val catetoAlQuadrato = (ipotenusa * ipotenusa) - (base * base);
  val cateto = Math.sqrt(catetoAlQuadrato);
  val a = angoloPoligono(lato, lati) / 2;
  val a2 = (90 - a);

  destra(180 - a2);
  colorePenna(rosso);
  val posizioneCorrente = posizione;
  avanti(cateto);
  disegnaCerchio(posizioneCorrente, cateto, blu);
  cateto;
}

def spostaInBasso(t: Tartaruga, v: Int) {
  t.colorePenna(nero);
  t.alzaPenna();
  t.indietro(v)
  t.abbassaPenna()
}

def disegnaPoligono(lato: Double, lati: Int) {
  val perimetroP = poligono(lato, lati);
  val angoloP = angoloPoligono(lato, lati);
  val raggioCerchioCircoscritto = disegnaRaggio(lato, lati);
  val apotema = disegnaApotema(lato, lati);

  val scriviDati = nuovaTartaruga(0, 0);
  scriviDati.ritardo(200);
  scriviDati.invisibile();

  spostaInBasso(scriviDati, 40);
  scriviDati.scrivi("angolo: " + angoloP.toString);

  spostaInBasso(scriviDati, 20);
  scriviDati.scrivi("perimetro (nero): " + perimetroP.toString);

  spostaInBasso(scriviDati, 20);
  scriviDati.scrivi("raggio (verde): " + Math.round(raggioCerchioCircoscritto).toString);

  spostaInBasso(scriviDati, 20);
  scriviDati.scrivi("apotema (rosso): " + Math.round(apotema).toString);
}

disegnaPoligono(120, 5);