
const applyToList = (f, l) =>  l.reduce(f);
const somma = (acc, valoreCorrente) => acc + valoreCorrente

applyToList(somma, [1,2,3,4,5]) // => 15
applyToList(somma, ["1",2,3,4,5]) // => 12345

