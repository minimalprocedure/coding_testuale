clear()

val f1 = () => {
  println("vado avanti")
  forward(100)  
}

val f2 = () => {
  println("mi giro a destra")
  right()
}

val esegui = Map(
  "avanti" -> f1,
  "destra" -> f2
  )

repeat(4) {
  esegui("avanti")()
  esegui("destra")()  
}

