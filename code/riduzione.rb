
# literal syntax
somma = ->(a, b) { a + b }

somma_norm_syntax = lambda { |a,b| a + b }
somma_norm_syntax_alt = lambda do |a,b| a + b end

def applica_la_funzione_alla_lista(f, lista)
  lista.reduce(&f)
end

res = applica_la_funzione_alla_lista(somma, [1,2,3,4,5])
puts res  